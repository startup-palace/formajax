# AJAX Form

Plugin to easily create AJAX forms. Can handle AJAX file uploads (currently tested with one file input, but may work with multiple input file).

## Required

- [jQuery](http://jquery.com/download/) (2.* recommanded)
- [Bootstrap 3.*](http://getbootstrap.com/)
- [blueimp/jQuery-File-Upload](https://github.com/blueimp/jQuery-File-Upload/) (optionnal)

## How To

Include necessary assets :

```html
<!-- Between head tag -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Bottom of the page -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="/path/to/your/assets/formajax/src/jquery.formajax.js" type="text/javascript"></script>
```

Then, run the script when the DOM is ready :

```javascript
$(function() {
	$('form.ajax').formajax();
});
```

## API

### setUrl

Set the request URL.

- Parameters :
	- url : new request URL
- Usage :

```javascript
$('#myForm').formajax('setUrl', '/foo/bar');
```

## Options

### always

Code executed after each AJAX call or if before function fail.

- Type : function
- Default :

```javascript
function() {}
```

### before

Code executed before each AJAX call.

- Type : function
- Default :
- Returns : `void` || [jQuery Deferred object](https://api.jquery.com/category/deferred-object/)

The jQuery Deferred object allows to create an asynchronous before function or/and throws an error in the before function and fail at the end of AJAX execution.

```javascript
function() {
	// Reinitialize form inputs
	$(this.settings.error, this.form).addClass('hidden').empty();

	$('.form-group.has-error', this).removeClass('has-error');
}
```

### button

CSS selector to handle button state change.

- Type : string
- Default : `` '.btn[type=submit]' ``

### done

AJAX success callback.

- Type : function
- Parameters :
	- data :
	- textStatus : String containing the textual status of the AJAX call
	- response : XHR object
- Default :

```javascript
function(data, textStatus, response) {
	if (data.redirect && data.redirect.length !== 0) {
		window.location.href = data.redirect;
	}
	else if (data.reload & data.reload == 1) {
		window.location.reload();
	}
}
```

### error

CSS selector to handle errors display.

- Type : string
- Default : `` '.error' ``

### fail

AJAX failure callback.

- Type : function
- Parameters :
	- response : XHR object
- Default :

```javascript
function(response) {
	var handler = '_' + response.status;

	if (typeof this.settings.http[handler] == 'function') {
		this.settings.http[handler].call(this, response);
	}
	else {
		alert(response.status + ' error');
	}
}
```

### formatErrors

Defines the error output formatting.

- Type : function
- Parameters :
	- errors : Array/object containing the errors
- Default :

```javascript
function(errors) {
	var html = '';

	if (typeof errors === 'string') {
		html.append($('<p>' + errors + '</p>'));
	}
	else {
		html = $('<ul></ul>');

		$.each(errors, function(index, error) {
			if ($(':input[name=' + index + ']', this.form).length !== 0) {
				$(':input[name=' + index + ']', this.form).parents('.form-group').addClass('has-error');
			}
			html.append($('<li>' + error + '</li>'));
		});
	}

	return html;
}
```

### getFormData

Defines how data is recovered form the form.

- Type : function
- Parameters :
	- form : Form on which the formajax is applied
- Default :

```javascript
function(form) {
	return form.serializeArray();
}
```

### getUrl

Defines how the URL for the request is defined.

- Type : function
- Parameters :
	- form : Form on which the formajax is applied
- Default :

```javascript
function(form) {
	return form.attr('action');
}
```

### http

Defines the handling of HTTP error codes.

- Type : object
- Default :

```javascript
{
	_400 : function(response) {
		var errors;

		if (response.responseJSON.errors && response.responseJSON.errors.length !== 0) {
			errors = response.responseJSON.errors;
		}
		else if (response.responseJSON.error && response.responseJSON.error.length !== 0) {
			errors = response.responseJSON.error;
		}
		else {
			errors = {};
		}

		var html = this.settings.formatErrors.call(this, errors);

		$(this.settings.error, this.form).append(html).removeClass('hidden');
	},
	_401 : function(response) {
		$(this.settings.error, this.form).append('You do not have the authorization to submit any content to this page - 401 error').removeClass('hidden');
	},
	_404 : function(response) {
		$(this.settings.error, this.form).append('The page you tried to submit to does not exists - 404 error').removeClass('hidden');
	},
	_500 : function(response) {
		$(this.settings.error, this.form).append('An server error occured - 500 error').removeClass('hidden');
	},
}
```

### uploadModal

CSS selector for the modal shown during file upload.

- Type : string
- Default : `` '.modal-upload' ``


## Dev

- install node.js & npm
- install gulp: `npm i -g gulp`
- install dev dependencies: `npm i`
- install Bower dependencies : `bower update`

Before pushing code:
- run gulp once: `gulp`
- run gulp in watch mode: `gulp watch`
