<?php

header('Content-Type: application/json');
http_response_code(200);

if (isset($_FILES['test_file'])) {
	$file = $_FILES['test_file'];

	echo json_encode(array('text' => 'Filename : ' . $file['name']));
}
else {
	echo json_encode(array('text' => 'No file sent.'));
}
