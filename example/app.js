$(function() {
	$('form#classic_form').formajax();

	$('form#file_form').formajax({
		done: function(data, textStatus, response) {
			$('.alert.alert-success', $(this.form)).text(data.text).removeClass('hidden');
		}
	});

	$('.select-http-code').on('change', function() {
		$(this).parents('form').formajax('setUrl', $(this).val());
	});
});
