/*!
  jQuery formajax plugin
  @name jquery.formajax.js
  @author Killian Blais (killian.blais@escaledigitale.com / @killianblais)
  @version 1.0.10
  @date 07/06/2016
  @category jQuery Plugin
  @license Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
*/
(function($) {

	$.formajax = function(form, options) {

		/**
		 * Default settings
		 */
		var defaults = {
			button : '.btn[type=submit]',
			error : '.error',
			uploadModal : '.modal-upload',
			http : {
				_400 : function(response) {
					var errors;

					if (response.responseJSON.errors && response.responseJSON.errors.length !== 0) {
						errors = response.responseJSON.errors;
					}
					else if (response.responseJSON.error && response.responseJSON.error.length !== 0) {
						errors = response.responseJSON.error;
					}
					else {
						errors = {};
					}

					var html = this.settings.formatErrors.call(this, errors);

					$(this.settings.error, this.form).append(html).removeClass('hidden');
				},
				_401 : function(response) {
					$(this.settings.error, this.form).append('You do not have the authorization to submit any content to this page - 401 error').removeClass('hidden');
				},
				_404 : function(response) {
					$(this.settings.error, this.form).append('The page you tried to submit to does not exists - 404 error').removeClass('hidden');
				},
				_500 : function(response) {
					$(this.settings.error, this.form).append('An server error occured - 500 error').removeClass('hidden');
				},
			},
			done : function(data, textStatus, response) {
				if (data.redirect && data.redirect.length !== 0) {
					window.location.href = data.redirect;
				}
				else if (data.reload && data.reload == 1) {
					window.location.reload();
				}
			},
			always : function() {},
			fail: function(response) {
				var handler = '_' + response.status;

				if (typeof this.settings.http[handler] == 'function') {
					this.settings.http[handler].call(this, response);
				}
				else {
					alert(response.status + ' error');
				}
			},
			before : function() {
				// Reinitialize form inputs
				$(this.settings.error, this.form).addClass('hidden').empty();

				$('.form-group.has-error', this.form).removeClass('has-error');
			},
			getUrl : function(form) {
				return form.attr('action');
			},
			getFormData : function(form) {
				return form.serializeArray();
			},
			formatErrors : function(errors) {
				var html = '';

				if (typeof errors === 'string') {
					html.append($('<p>' + errors + '</p>'));
				}
				else {
					html = $('<ul></ul>');

					$.each(errors, function(index, error) {
						if ($(':input[name=' + index + ']', this.form).length !== 0) {
							$(':input[name=' + index + ']', this.form).parents('.form-group').addClass('has-error');
						}
						html.append($('<li>' + error + '</li>'));
					});
				}

				return html;
			},
		};

		var plugin = this;

		plugin.settings = {};

		var $form = plugin.form = $(form);

		/**
		 * Initialize the plugin
		 */
		plugin.init = function() {
			plugin.settings = $.extend(true, {}, defaults, options);

			plugin.url = plugin.settings.getUrl($form);

			if ($.fn.fileupload && $('input:file', $form).length == 1) {
				$('input:file', $form).fileupload({
					formData: [],
					dataType: 'json',
					autoUpload: false,
					beforeSend: function(e) {
						this.url = plugin.url;
						$(plugin.settings.uploadModal).modal('show');
					},
					ajaxComplete: function(e) {
						$(plugin.settings.uploadModal).modal('hide');
					},
					fail: function(e, data) {
						fail(data._response.jqXHR);
					},
					done: function(e, data) {
						done(data._response.result, data.textStatus, data._response.jqXHR);
					},
					always: always,
				})
				.bind('fileuploadadd', function(event, data) {
					// Remove previous 'submit' event from form
					$($form).off('submit');

					$($form).on('submit', function(event) {
						event.preventDefault();

						$.when(before()).then(function() {
							data.submit();
						}, function() {
							always();
						});
						
					});
				})
				.bind('fileuploadsubmit', function(event, data) {
					data.formData = plugin.settings.getFormData($form);
				});
			}

			$form.on('submit', function(event) {
				event.preventDefault();

				$.when(before()).then(function() {
					var posting = $.post(plugin.url, plugin.settings.getFormData($form));

					posting.fail(fail);
					posting.done(done);
				}, function() {
					always();
				});
			});
		};

		var done = function(data, textStatus, response) {
			$(plugin.settings.button, plugin.form).button('done');

			setTimeout(function() {
				always();
			}, 2000);

			plugin.settings.done.call(plugin, data, textStatus, response);
		};

		var always = function() {
			$(plugin.settings.button, plugin.form).button('reset');

			plugin.settings.always.call(plugin);
		};

		var fail = function(response) {
			always();
			plugin.settings.fail.call(plugin, response);
		};

		var before = function() {
			$(plugin.settings.button, plugin.form).button('loading');

			return plugin.settings.before.call(plugin);
		};

		plugin.setUrl = function(url) {
			plugin.url = url;
		};

		plugin.init();
	};

	$.fn.formajax = function(options) {
		var args = arguments;

		return this.each(function() {
			if (undefined === $(this).data('formajax')) {
				var plugin = new $.formajax(this, options);
				$(this).data('formajax', plugin);
			}
			else if (typeof options === 'string') {
				var form = $(this).data('formajax');
				if (typeof form[options] === 'function') {
					form[options].apply(form, Array.prototype.slice.call(args, 1));
				}
				else {
					console.error('The function `' + options + '` does not exists.');
				}
			}
		});
	};

})(jQuery);
