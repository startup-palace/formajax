/* jshint globalstrict: true, node: true */
'use strict';

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var jsFiles = ['src/*.js'];

gulp.task('lintJS', function() {
	return gulp.src(jsFiles)
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(jshint.reporter('fail'));
});

gulp.task('compress', function() {
	gulp.src(jsFiles)
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('dist'))
});

function eventLogger(event) {
	console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
}

gulp.task('watchJS', function() {
	var watcher = gulp.watch(jsFiles, ['lintJS']);
	watcher.on('change', eventLogger);
	return watcher;
});

gulp.task('default', ['lintJS', 'compress']);
gulp.task('watch', ['lintJS', 'watchJS']);
